import React from "react";
import '../style/Login.css';
import { useNavigate } from 'react-router-dom';

function Login (){
    const Navigate = useNavigate();
    const [user,setUser] = React.useState({}) ;
    const [users,setUsers] = React.useState([{"username":"aaa","password":"aaa"}]) ;
    const [flash,setFlash] = React.useState("") ;

    const handleLogin = async (event) => {
        event.preventDefault() ;
        if(user.username && user.password){
            // there should exist a backend to which we send requests
            //axios.post(.......)
            if(users.some(storedUser => storedUser.username === user.username 
                && storedUser.password === user.password )){
                setFlash("") ;
                Navigate('/templates') ;
                window.location.reload() ;
            }
            else{
                setFlash("invalid username or password : try 'aaa' as a username and password ") ;
            }
        }
        else{
            setFlash("Please fill in the fields") ;
        }
    }

    const onFormChange = (event) => {
        const userTemp = user;
        userTemp[event.target.name] = event.target.value;
        setUser(userTemp);
    }

    return (
        <div className="container">
            <br />
            <div className="card text-white bg-dark">
            <div className="card-header">
                Login
            </div>
                <div className="card-body">
                    <form className="form-row" onChange={event => onFormChange(event)}>
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input type="text" name="username" id="" className="form-control"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input type="text" name="password" id="" className="form-control"/>
                        </div>
                        <br/>
                        <button type="submit" onClick={event => handleLogin(event)} className="btn btn-success btn-lg btn-block">Sign in</button>
                    </form>
                    <br />
                    {(flash != "") ?
                        <div className="alert alert-danger" role="alert">
                            {flash}
                        </div>
                        : null
                    }
                </div>
            </div>
        </div>
    );

}
export default Login;