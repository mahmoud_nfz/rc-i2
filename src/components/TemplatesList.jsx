import React from "react";
import { Navigate, useNavigate, Link } from 'react-router-dom';
import { rawHTML } from "../templates/template";

function TemplatesList (){
    const [user,setUser] = React.useState({}) ;
    const [users,setUsers] = React.useState([{"username":"aaa","password":"bbb"}]) ;
    const [flash,setFlash] = React.useState("") ;
    const [templates,setTemplates] = React.useState([]) ;

    React.useEffect(() => {
        let temps = [] ;
        for(let i=0;i<10;i++){
            temps [i] = 
                    <Link to={"/template/"+(i+1)}>
                        <div style={styleObj} className="card">
                            <div className="card-header">Template {i+1}</div>
                            <div dangerouslySetInnerHTML={{ __html: rawHTML }}></div>
                        </div>
                    </Link>
        }
        setTemplates(temps) ;
      },[]);


    

const styleObj = {

    fontSize: 14,

    color: "#4a54f1",

    textAlign: "center",

    display : "inline-block",
    margin : "10px",
    // paddingTop: "100px", 
    "maxWidth" : "400px" , 
}
    let i = 1 ;
    
    return (
        <div className="container">
            <h3 className="text-danger">These Are the available templates</h3>
            {templates}
        </div>
    );

}
export default TemplatesList;