import React from "react";
import { rawHTML } from "../templates/template";

export function EditTemplate(){
    
    return (
        <div className="container">
            <div class=" card form-group">
                <label for="comment">Editing Template {window.location.href.slice(-1)}</label>
                <textarea class="form-control" rows="50" id="comment">{rawHTML}</textarea>
            </div> 
        </div>
    );

}