import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Routes, Route, Link} from "react-router-dom";
import Login from './components/Login';
import TemplatesList from './components/TemplatesList';
import { EditTemplate } from './components/EditTemplate';



function App() {
  return (
    <Router>
        {/* <Navbar user={user}/> */}
        <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/templates" element={<TemplatesList />} />
            <Route path="/template/:id" element={<EditTemplate />} />
        </Routes>
    </Router>
  );
}

export default App;
